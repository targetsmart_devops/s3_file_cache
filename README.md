# s3_file_cache

Cache successful workflow output file results to s3 based on the checksum of a
workflow input file.


## Installation

``` sh
pip install git+https://git@bitbucket.org/targetsmart_devops/s3_file_cache
```

## Choosing Buckets and Prefixes


You can choose any bucket and prefix to store the cache.
Here is a typical usage for the ts-batch-io projects:

``` python
    DEFAULT_FILE_CACHE_BUCKET = "ts-batch-io"
    DEFAULT_FILE_CACHE_PREFIX = "cache/[project_name]"
```

## Usage

Upon successful workflow completion, store a JSON object to S3 that represents a cache entry associating the output path with the input path.

``` python
from s3_file_cache import S3FileCache

cache = S3FileCache(S3_BUCKET, S3_KEY_PREFIX)
stored_entry = cache.store_success(workflow_input_path, workflow_output_path)

```

Later, check if an input file has already been successfully processed in order to reuse
workflow output:

``` python
from s3_file_cache import S3FileCache
cache = S3FileCache(S3_BUCKET, S3_KEY_PREFIX)

found_cache_entry = cache.is_cached(workflow_input_path)
cache.download(found_cache_entry['data_key'], local_path)
```

If a cached entry is not found, `found_cache_entry` will be `None`. When a
cached entry is found, the following dict keys are available:


- `workflow_input_path`: The input path that was passed to `store_success` when creating the entry
- `workflow_output_path`: The output path that was passed to `store_success` when creating the entry
- `entry_key`: The S3 key where the cache entry has been stored
- `data_key`: The S3 key where the successful output file contents have been copied. Download this object to reuse the previously cached file.
- `input_checksum`: The MD5 checksum hexdigest string calculated by `store_success` when creating the entry
- `stored_at`: `datetime.datetime` object representing the storage time (naive UTC)
- `prefix`: The s3 key prefix string that was passed to `store_success` when creating the entry
- `extras`: list of str values or None, based on the argument passed to `store_success` when creating the entry
- `bucket`: The s3 bucket name passed in the `S3FileCache` constructor

If a cached entry is not found, `found_cache_entry` will be `None`.

While less commonly needed, in some cases you may wish to cache results based on
extra information in addition to the input file checksum. Use the `extras`
argument to `is_cached` and `store_success` to pass a list of string values to
include in the checksum calculation.


## Cache expiration

Either use S3 lifecycle rules to delete cache entries after some period of time
or utilize the `stored_at` timestamp in the cached result object that is returned
by `is_cached`.
