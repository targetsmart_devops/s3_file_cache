"""Routines for caching batch data processing results at the file-level using S3 storage."""

from __future__ import annotations

import datetime
import hashlib
import json
import logging
import os
from typing import TYPE_CHECKING
from urllib.parse import urlparse

import boto3
from smart_open import open as sopen

if TYPE_CHECKING:
    from collections.abc import Sequence


SEPARATOR = "|"

MD5_BLOCK_SIZE = 4096  # Read in chunks of 4 KB
S3_START = "s3://"


def _parse_s3(path: str) -> tuple[str, str]:
    """
    Given an s3 path, return the bucket and key.
    """
    if not path.startswith("s3://"):
        msg = f'Expected an s3 path, but got "{path}"'
        raise ValueError(msg)
    parsed = urlparse(path)
    return str(parsed.netloc), str(parsed.path.lstrip("/"))


def _etag_from_s3_head(s3_path: str) -> str:
    """
    Given an s3 path, return the etag from the HEAD.

    Note that for small, non-encrypted payloads and for copy operations the etag is the md5 of the object.
    BUT for multipart uploads, each part has its own md5, and the etag shown is the md5 of all the md5s together.

    This means if you multipart upload an object, it has one particular etag.
    If you multipart upload the same file in the same fashion (same part size), it appears to have the same etag as before.
    But if you then copy it from one s3 location to another, the etag changes to the md5 of the entire file.

    So for use cases where the file is always uploaded in the same manner, using the S3 etag as a cache key
    appears to suffice. We should be on the lookout if we are seeing cache misses where we expected hits.

    See https://docs.aws.amazon.com/AmazonS3/latest/userguide/checking-object-integrity.html#checking-object-integrity-etag-and-md5
    and https://docs.aws.amazon.com/AmazonS3/latest/userguide/checking-object-integrity.html#large-object-checksums
    """
    client = boto3.client("s3", region_name="us-east-1")
    bucket, key = _parse_s3(s3_path)

    res = client.head_object(Bucket=bucket, Key=key)

    # Extract the ETag header
    # ETag is surrounded by double quotes, so we remove them
    return str(res["ETag"]).strip('"')


def _local_md5sum(filename: str) -> str:
    """
    Return the md5sum of a local file.
    """
    if filename.startswith(S3_START):
        msg = "This function only supported for local paths"
        raise ValueError(msg)

    md5 = hashlib.md5()  # noqa: S324

    with open(filename, "rb") as file:
        for chunk in iter(lambda: file.read(MD5_BLOCK_SIZE), b""):
            md5.update(chunk)

    return md5.hexdigest()


def checksum(filename: str, extras: Sequence[str] | None = None) -> str:
    """Return a checksum of sorts for a particular file.

    WHY EXTRAS
    The reason for supporting the `extras` argument is to support cases where
    the job parameters can result in different outputs.  See this example
    from the exacttrack implementation:
    https://bitbucket.org/targetsmart_devops/exacttrack_batch_io/src/25b22b8c743fba2883bd091a23493d19a73460d5/exacttrack_batch_io/workflow/infutor.py?at=production#infutor.py-171
    """
    # For s3 paths, start with the md5 from S3 HEAD, since that is an inexpensive operation
    # for local paths, compute the md5
    etag = (
        _etag_from_s3_head(filename)
        if filename.startswith(S3_START)
        else _local_md5sum(filename)
    )
    if not extras:
        logging.info(f"Checksum: using etag {etag} from S3 HEAD for {filename}")
        return etag

    # Avoid hashing the whole file because for large files that takes a long time
    things_to_hash = [etag, *extras]
    # Join with SEPARATOR so that ['Orange', 'Bananas'] and ['OrangeB', 'ananas'] give different hashed output
    things_to_hash_str = SEPARATOR.join(things_to_hash)

    m = hashlib.md5()  # noqa: S324
    m.update(things_to_hash_str.encode("utf-8"))
    digest = m.hexdigest()

    logging.info(
        f"Checksum: using md5 {digest} computed with from etag and extras {filename}, {etag}, {extras}"
    )
    return digest


class S3FileCache:
    """Provides caching methods."""

    def __init__(
        self, s3_bucket: str, s3_key_prefix: str, region_name: str = "us-east-1"
    ) -> None:
        """Init.

        Parameters
        ----------
        s3_bucket : str
                S3 bucket where JSON cache entry files will be stored
        s3_key_prefix: str
                S3 Key prefix where JSON cache entry files will be stored
        region_name: str, optional
                AWS region name
        """
        self.s3_bucket: str = s3_bucket
        self.s3_key_prefix: str = s3_key_prefix
        self.s3_client = boto3.client("s3", region_name=region_name)

    def is_cached(
        self, workflow_input_path: str, extras: Sequence[str] | None = None
    ) -> dict[str, str | Sequence[str] | datetime.datetime] | None:
        """
        Check if there is a cache entry for `workflow_input_path`.

        Parameters
        ----------
        workflow_input_path: str
                local or S3 path of input file name. The contents of this file will be used to find associated cached results.

        Other Parameters
        ----------------
        extras: sequence of str values, optional
                Optionally include these str values in the checksum calculation in addition to input file contents

        Returns:
        -------
        contents : dict or None
                If None, then no cached entry was found. Dict result indicates a found result.

        Notes:
        -----
        Keys found in the returned `contents`:

        - `workflow_input_path`: The input path that was passed to `store_success` when creating the entry
        - `workflow_output_path`: The output path that was passed to `store_success` when creating the entry
        - `input_checksum`: The sha256 checksum hexdigest string calculated by `store_success` when creating the entry
        - `stored_at`: `datetime.datetime` object representing the storage time (naive UTC)
        - `prefix`: The s3 key prefix string that was passed to `store_success` when creating the entry
        - `extras`: list of str values or None, based on the argument passed to `store_success` when creating the entry
        - `bucket`: The s3 bucket name passed in the `S3FileCache` constructor

        Use `stored_at` and `workflow_output_path` to determine if a previous
        workflow result file can be reused.

        """
        input_checksum = checksum(workflow_input_path, extras)

        keyname = f"{self.s3_key_prefix}/__entries/{input_checksum}"

        logging.info(
            f"Searching for cached entry for filename {workflow_input_path}. Bucket: {self.s3_bucket}. Key: {keyname}"
        )
        try:
            resp = self.s3_client.get_object(Bucket=self.s3_bucket, Key=keyname)
        except self.s3_client.exceptions.NoSuchKey:
            logging.info(f"No cached result was found for {workflow_input_path}")
            return None

        contents: dict[str, str | Sequence[str] | datetime.datetime] = json.loads(
            resp["Body"].read().decode("utf8")
        )
        if contents:
            logging.info(
                f"Found stored entry for {workflow_input_path}. Contents: {contents}"
            )
            contents["stored_at"] = datetime.datetime.fromisoformat(
                str(contents["stored_at"])
            )

        return contents

    def store_success(
        self,
        workflow_input_path: str,
        workflow_output_path: str,
        extras: Sequence[str] | None = None,
    ) -> dict[str, str | list[str] | None] | None:
        """Store a cache entry.

        Parameters
        ----------
        workflow_input_path: str
                local or S3 path of input file name. The contents of this file will be used to find associated cached results.

        workflow_output_path: str
                local or S3 path of output file name. This path will be stored in the cache entry JSON.

        Other Parameters
        ----------------
        extras: sequence of str values, optional
                Optionally include these str values in the checksum calculation in addition to input file contents

        """
        if workflow_input_path == workflow_output_path:
            # Cache is ineffective when input_path == output_path
            # because it re-computes the input_checksum
            # and gets a different answer after the input file
            # has been overwritten
            logging.warning(
                "Not storing in cache because input path and output path are the same",
            )
            return None

        input_checksum = checksum(workflow_input_path, extras=extras)

        entry_keyname = f"{self.s3_key_prefix}/__entries/{input_checksum}"
        data_keyname = f"{self.s3_key_prefix}/__data/{input_checksum}"

        with sopen(workflow_output_path, "rb", compression="disable") as reader:
            with sopen(f"s3://{self.s3_bucket}/{data_keyname}", "wb") as writer:
                for chunk in reader:
                    writer.write(chunk)

        contents = {
            "workflow_input_path": workflow_input_path,
            "workflow_output_path": workflow_output_path,
            "entry_key": entry_keyname,
            "data_key": data_keyname,
            "input_checksum": input_checksum,
            "stored_at": datetime.datetime.now(datetime.timezone.utc).isoformat(),
            "prefix": self.s3_key_prefix,
            "extras": list(extras) if extras else None,
            "bucket": self.s3_bucket,
        }

        self.s3_client.put_object(
            Bucket=self.s3_bucket, Key=entry_keyname, Body=json.dumps(contents)
        )
        logging.info(
            f"Stored entry for {workflow_input_path}. Entry Key: {entry_keyname}. Data Key: {data_keyname}. Contents: {contents}"
        )
        return contents

    def download(self, data_key: str, local_path: str) -> None:
        """
        Download the cached data file from S3.

            cache.download(found_cache_entry['data_key']), local_path)
        """
        if local_path.startswith(S3_START):
            with sopen(f"s3://{self.s3_bucket}/{data_key}", "rb", compression="disable") as reader:
                with sopen(local_path, "wb") as writer:
                    for data in reader:
                        writer.write(data)
        else:
            self.s3_client.download_file(self.s3_bucket, data_key, local_path)


if __name__ == "__main__":
    path_ = os.getenv("ANY_S3_PATH")
    if not path_:
        errmsg = "ANY_S3_PATH environment variable not set."
        raise ValueError(errmsg)
    print(_parse_s3(path_))
    print(_etag_from_s3_head(path_))

    print("\ns3 path:")
    print(checksum(path_))
    print(checksum(path_, ["hi", "there"]))

    print("\nlocal path:")
    print(_local_md5sum("demo.py"))
    print(checksum("demo.py"))
    print(checksum("demo.py", ["hi", "there"]))
