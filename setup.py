"""Package setup."""

from setuptools import setup

setup(
    name="s3_file_cache",
    version="0.3.3",
    description="Cache successful workflow output file results to s3 based on checksum of workflow input file.",
    author="TargetSmart",
    author_email="devops@targetsmart.com",
    license="Copyright TargetSmart. All rights reserved.",
    url="https://bitbucket.org/targetsmart_devops/s3_file_cache.git",
    download_url="https://bitbucket.org/targetsmart_devops/s3_file_cache.git",
    test_suite="tests",
    py_modules=["s3_file_cache"],
    install_requires=["boto3", "smart_open[s3]"],
)
