"""Test s3_file_cache.py."""

# ruff: noqa: S101, ANN001, ANN201, ANN202
# mypy: ignore-errors

import datetime
import json
from io import BytesIO

import boto3
from freezegun import freeze_time

from s3_file_cache import S3FileCache

S3_BUCKET = "mock-cache-storage-bucket"
S3_KEY_PREFIX = "my_workflows/prefix"


def test_cached_result_found(mock_aws) -> None:
    """Test that cached results are found, mocking S3 interaction."""
    cached_result = {
        "workflow_input_path": "input.csv",
        "workflow_output_path": "output.csv",
        "input_checksum": "123",
        "stored_at": datetime.datetime.now(datetime.timezone.utc).isoformat(),
    }

    def mock_aws_replacement(operation_name, kwargs):
        if operation_name == "GetObject":
            if kwargs["Key"] == "input.csv":
                return {
                    "Body": BytesIO(b"test"),
                    "ContentLength": 4,
                    "ResponseMetadata": {"RetryAttempts": 3, "HTTPStatusCode": 200},
                    "ContentRange": "bytes 0-9/43",
                }
            return {
                "Body": BytesIO(json.dumps(cached_result).encode("utf8")),
                "ContentLength": 4,
                "ResponseMetadata": {"HTTPStatusCode": 200},
            }

        if operation_name == "HeadObject":
            return {"ETag": "foo"}

        errmsg = f"You need to mock boto3.{operation_name}"
        raise RuntimeError(errmsg)

    mock_aws.side_effect = mock_aws_replacement
    cache = S3FileCache(S3_BUCKET, S3_KEY_PREFIX)
    found_cached_result = cache.is_cached("s3://mock-input-bucket/input.csv")
    assert found_cached_result is not None
    assert isinstance(found_cached_result["stored_at"], datetime.datetime)
    found_cached_result["stored_at"] = found_cached_result["stored_at"].isoformat()
    assert found_cached_result == cached_result


def test_cached_result_not_found(mock_aws) -> None:
    """Test when value is not found."""

    def mock_aws_replacement(operation_name, kwargs):
        if operation_name == "GetObject":
            if kwargs["Key"] == "input.csv":
                return {
                    "Body": BytesIO(b"test"),
                    "ContentLength": 4,
                    "ResponseMetadata": {"RetryAttempts": 3},
                    "ContentRange": "bytes 0-9/43",
                }

            raise boto3.client("s3").exceptions.NoSuchKey(
                {"Error": {"Code": 1, "Message": "missing"}}, "GetObject"
            )
        if operation_name == "HeadObject":
            return {"ETag": "foo"}

        errmsg = f"You need to mock boto3.{operation_name}"
        raise RuntimeError(errmsg)

    mock_aws.side_effect = mock_aws_replacement
    cache = S3FileCache(S3_BUCKET, S3_KEY_PREFIX)
    found_cached_result = cache.is_cached("s3://mock-input-bucket/input.csv")
    assert not found_cached_result


@freeze_time("2012-01-14")
def test_store_success(mock_aws):
    """Test successful cache storage."""

    def mock_aws_replacement(operation_name, kwargs):  # noqa: PLR0911
        if operation_name == "GetObject":
            if kwargs["Key"] == "input.csv":
                return {
                    "Body": BytesIO(b"test"),
                    "ContentLength": 4,
                    "ResponseMetadata": {"RetryAttempts": 3, "HTTPStatusCode": 200},
                    "ContentRange": "bytes 0-9/43",
                }
            if kwargs["Key"] == "output.csv":
                return {
                    "Body": BytesIO(b"test"),
                    "ContentLength": 4,
                    "ResponseMetadata": {"RetryAttempts": 3, "HTTPStatusCode": 200},
                    "ContentRange": "bytes 0-9/43",
                }
        if operation_name == "PutObject":
            assert kwargs["Key"].startswith("my_workflows/prefix/")
            return {}
        if operation_name == "CreateMultipartUpload":
            return {"UploadId": 1}
        if operation_name == "UploadPart":
            return {"ETag": "foo"}
        if operation_name == "CompleteMultipartUpload":
            return {}
        if operation_name == "HeadObject":
            return {"ETag": "foo"}

        errmsg = f"You need to mock boto3.{operation_name}"
        raise RuntimeError(errmsg)

    mock_aws.side_effect = mock_aws_replacement
    cache = S3FileCache(S3_BUCKET, S3_KEY_PREFIX)
    stored_cached_result = cache.store_success(
        "s3://mock-input-bucket/input.csv",
        "s3://mock-input-bucket/output.csv",
        extras=["foo,bar", "page=4"],
    )
    input_checksum = "08a74ae3d2e4786067b05837317577b8"

    assert stored_cached_result == {
        "bucket": S3_BUCKET,
        "stored_at": "2012-01-14T00:00:00+00:00",
        "extras": ["foo,bar", "page=4"],
        "input_checksum": input_checksum,
        "data_key": f"my_workflows/prefix/__data/{input_checksum}",
        "entry_key": f"my_workflows/prefix/__entries/{input_checksum}",
        "prefix": S3_KEY_PREFIX,
        "workflow_input_path": "s3://mock-input-bucket/input.csv",
        "workflow_output_path": "s3://mock-input-bucket/output.csv",
    }
