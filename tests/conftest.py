"""Pytest setup."""

# ruff: noqa: ANN001, ANN201, ANN202
# mypy: ignore-errors

import pytest


@pytest.fixture()
def mock_aws(mocker):
    """Fixture to mock interaction with S3.
    """
    return mocker.patch("botocore.client.BaseClient._make_api_call")
